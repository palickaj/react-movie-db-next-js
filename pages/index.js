import { Component } from 'react';

import List from '../components/List';
import Layout from '../components/Layout';
import LoadingBox from '../components/LoadingBox';
import ErrorBox from '../components/ErrorBox';

import handleApiLoad from '../functions/handleApiLoad';

class Index extends Component {
	constructor(props) {
		super(props);

		this.state = {
			config: [],
			loadingConfig: true,
			movies: [],
			loadingMovies: true,
			series: [],
			loadingSeries: true,
			family: [],
			loadingFamily: true,
			documentary: [],
			loadingDocumentary: true,
			fullyLoaded: false,
			inError: false,
		};
	}

	async componentDidMount() {
		await handleApiLoad('https://api.themoviedb.org/3/configuration?api_key=88bedc72e8eebbce32b02966034d438c')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					config: data,
					loadingConfig: false,
				});
			});

		await handleApiLoad('https://api.themoviedb.org/3/movie/popular?api_key=88bedc72e8eebbce32b02966034d438c&page=1', 'results')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					movies: data,
					loadingMovies: false,
				});
			});

		await handleApiLoad('https://api.themoviedb.org/3/tv/popular?api_key=88bedc72e8eebbce32b02966034d438c&page=1', 'results')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					series: data,
					loadingSeries: false,
				});
			});

		await handleApiLoad('https://api.themoviedb.org/3/discover/movie?api_key=88bedc72e8eebbce32b02966034d438c&sort_by=popularity.desc&page=1&with_genres=10751', 'results')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					family: data,
					loadingFamily: false,
				});
			});


		await handleApiLoad('https://api.themoviedb.org/3/discover/tv?api_key=88bedc72e8eebbce32b02966034d438c&sort_by=popularity.desc&page=1&with_genres=99', 'results')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					documentary: data,
					loadingDocumentary: false,
				});
			});

		if (!this.state.loadingConfig && !this.state.loadingDocumentary && !this.state.loadingFamily &&
			!this.state.loadingMovies && !this.state.loadingSeries && !this.state.inError) {
			this.setState({
				fullyLoaded: true
			})
		}
	}


	render() {
		return (
			<>
				<Layout>
					{!this.state.fullyLoaded
						?
						this.state.inError
							?
							<ErrorBox />
							:
							<LoadingBox />
						:
						<>
							<List movieList={this.state.movies} config={this.state.config} type="movie" title="Popular movies" />
							<List movieList={this.state.series} config={this.state.config} type="tv" title="Popular series" />
							<List movieList={this.state.family} config={this.state.config} type="movie" title="Family" />
							<List movieList={this.state.documentary} config={this.state.config} type="tv" title="Documentary" />
						</>
					}
				</Layout>
			</>
		);
	}
}

export default Index