import { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import { withRouter } from 'next/router'

import Layout from '../../components/Layout';
import ShowMovie from '../../components/ShowMovie';
import ShowSerie from '../../components/ShowSerie';
import LoadingBox from '../../components/LoadingBox';
import ErrorBox from '../../components/ErrorBox';

import handleApiLoad from '../../functions/handleApiLoad';

class Detail extends Component {

	static async getInitialProps() {
		return {};
	}

	constructor(props) {
		super(props);

		this.state = {
			movieId: props.router.query.id,
			type: props.router.query.type,
			loadingEntry: true,
			entry: [],
			loadingConfig: true,
			config: [],
			fullyLoaded: false,
			inError: false,
		}
	}

	async componentDidMount() {
		await handleApiLoad('https://api.themoviedb.org/3/configuration?api_key=88bedc72e8eebbce32b02966034d438c')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					config: data,
					loadingConfig: false,
				});
			});

		await handleApiLoad(`https://api.themoviedb.org/3/${this.state.type}/${this.state.movieId}?api_key=88bedc72e8eebbce32b02966034d438c`)
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					entry: data,
					loadingEntry: false,
				});
			});

		if (!this.state.loadingConfig && !this.state.loadingEntry && !this.state.inError) {
			this.setState({
				fullyLoaded: true,
			});
		}
	}

	render() {
		return(
			<Layout>
				{!this.state.fullyLoaded
					? <LoadingBox />
					:
					<>
						{this.state.type === "movie"
							? <ShowMovie item={this.state.entry} config={this.state.config} />
							: <ShowSerie item={this.state.entry} config={this.state.config} />
						}
					</>
				}
			</Layout>
		)
	}
}

export default withRouter(Detail);