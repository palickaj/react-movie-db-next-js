import Layout from '../components/Layout';
import { Component } from 'react';
import List from '../components/List';
import LoadingBox from '../components/LoadingBox';
import ErrorBox from '../components/ErrorBox';

import handleApiLoad from '../functions/handleApiLoad';

class Search extends Component {
	state = {
		query: '',
		loadingConfig: true,
		config: [],
		loadingMovies: false,
		movies: [],
		loadingSeries: false,
		series: [],
		loading: false,
		inError: false,
	}

	handleInputChange = async () => {
		this.setState({
			query: this.search.value
		}, async () => {
			if (this.state.query && this.state.query.length > 1) {
				if (this.state.query.length % 2 === 0) {
					this.setState({
						loading: true,
						loadingMovies: true,
						loadingSeries: true,
					});

					await handleApiLoad(`https://api.themoviedb.org/3/search/movie?api_key=88bedc72e8eebbce32b02966034d438c&language=en-US&page=1&include_adult=false&query=${this.state.query}`, "results")
						.catch(() => {
							this.setState({
								inError: true
							});
						})
						.then((data) => {
							this.setState({
								movies: data,
								loadingMovies: false,
							});
						});

					await handleApiLoad(`https://api.themoviedb.org/3/search/tv?api_key=88bedc72e8eebbce32b02966034d438c&language=en-US&page=1&include_adult=false&query=${this.state.query}`, "results")
						.catch(() => {
							this.setState({
								inError: true
							});
						})
						.then((data) => {
							this.setState({
								series: data,
								loadingSeries: false,
							});
						});

					if (!this.state.loadingMovies && !this.state.loadingSeries && !this.state.inError) {
						this.setState({
							loading: false,
						});
					}
				}
			} else if (!this.state.query) {
			}
		});
	}

	async componentDidMount() {
		await handleApiLoad('https://api.themoviedb.org/3/configuration?api_key=88bedc72e8eebbce32b02966034d438c')
			.catch(() => {
				this.setState({
					inError: true
				});
			})
			.then((data) => {
				this.setState({
					config: data,
					loadingConfig: false,
				});
			});
	}

	render() {
		return (
			<Layout>
				<div className="row">
					<div className="col-12">
						<form id="search-form">
							<div className="input-group">
								<div className="input-group-prepend">
									<div className="input-group-text">
										<i className="fas fa-search"></i>
									</div>
								</div>
								<input
									placeholder="Search for..."
									className="form-control search-box form-control-lg"
									ref={input => this.search = input}
									onChange={this.handleInputChange}
								/>
							</div>
						</form>
					</div>
				</div>

				<div className="row">
					<div className="col-12">
						{this.state.inError
							?
								<ErrorBox />
							:
								<>
								{this.state.loading
									?
										<LoadingBox />
									:
										<>
											{this.state.movies && this.state.movies.length > 0 &&
												<List movieList={this.state.movies} config={this.state.config} type="movie" title={"Movies matching '" + this.state.query + "'"} />
											}
											{this.state.series && this.state.series.length > 0 &&
												<List movieList={this.state.series} config={this.state.config} type="tv" title={"Series matching '" + this.state.query + "'"} />
											}
										</>
								}
								</>
						}
					</div>
				</div>
			</Layout>
		)
	}
}

export default Search;