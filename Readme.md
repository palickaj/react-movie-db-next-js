# Použité frameworky

V tomto úkolu byl využit React framework `Next.js`. Jako CSS framework byl použit `Bootstrap`. Pro zobrazení videa je určen `Shaka-video-player`.

# Struktura složek
Struktura složek respektuje doporučení stanovená frameworkem `Next.js`.

* Jednotlivé komponenty jsou ve složce `components`.
* Jednotlivé stránky jsou umístěny ve složce `pages`, resp. v jejich podložkách.
* Sdílené funkce jsou umístěny ve složce `functionos`.


# Popis jednotlivých komponent
V následující části budou popsány jednotlivé komponenty.
* ErrorBox - tato komponenta je určená pro zobrazení v případě chyby.
* Layout - tato komponenta definuje základní layout stánky (titulek, linkování CSS, navigace).
* List - tato komponenta vytváří grafickou reprezentaci seznamu filmů (seriálů) z API.
* LoadingBox - tato komponenta se zobrazuje v době získávání informací z veřejného API.
* Navbar - tato komponenta zobrazuje hlavní navigační panel.
* ResponsiveImage - tato komponenta je určena pro responzivní zobrazení obrázků.
* ShowMovie - tato komponenta slouží pro zobrazení informací o jednotlivých filmech.
* ShowSerie - tato komponenta slouží pro zobrazení informací o jednotlivých TV sériích.
* VideoModalWindow - tato komponenta slouží pro zobrazení modálního okna s Shaka-video player.

V případě, kdy některá z komponent vyžaduje konfiguraci API, je tato konfigurace předána do komponenty pomocí `props`.

# Popis jednotlivých komponent
V následující části budou popsány jednotlivé stránky.
* index.js - slouží pro zobrazení hlavní stánky a zobrazení seznamů filmů a TV seriálů.
* search.js - slouží pro zobrazení vyhledávacího pole výsledků vyhledávání.
* [type]/[id].js - slouží pro zobrazení konkrétního filmu (TV seriálu), kde [type] může nabývat hodnot movie/tv a [id] určuje ID média v databázi www.themoviedb.org.

V případě, kdy některá ze stránek vyžaduje konfiguraci API, je tato konfigurace načtena přímo stránkou pomocí funkce `fetch`.

# Sdílené funkce
Tato složka obsahuje jedinou funci, a to `handleApiLoad`. Ta se stará o načtení konkrétní informace z api. Její předpis je následující:
```js
const handleApiLoad = async (url, subArrayResult = '')
```

Funkce jeden povinný parametr `url` a jeden nepovinný parametr `subArrayResult`.

* Parametr `url` obsahuje adresu API, která má být získána pomocí JavaScript funkce `fetch`.
* Nepovinný parametr `subArrayResult` určuje, kterou část získaného pole má funkce vrátit.

# Instalace
Pro nainstalování a zprovoznění tohoto úkolu stačí následující kroky.

* Pomocí příkazu `yarn install` v kořenovém adresáři úkolu dojde k instalaci všech součástí a potřebných knihoven.
* Pomocí příkazu `yarn run dev` dojde ke spuštění lokálního serveru.