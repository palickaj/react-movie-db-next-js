/**
 *
 * @param {*} url URL of resource to be fetched
 * @param [optional] {*} subArrayResult Name of entry to be returned from fetched JSON
 */
const handleApiLoad = async (url, subArrayResult = '') => {
	try {
		const res = await fetch(url);
		if (!res.ok) {
			throw Error(res.statusText);
		}

		const json = await res.json();
		if (subArrayResult === '')
			return json;
		else
			return json[subArrayResult]
	} catch (error) {
		throw Error(error);
	}
}

export default handleApiLoad;