import React, { Component } from 'react';
import VideoModalWindow from './VideoModalWindow';

import ResponsiveImage from './ResponsiveImage';

/**
 * This class creates component handling Movie info page
 */
class ShowMovie extends Component {
	/**
	 * Class constructor
	 * @param {*} props Class properties
	 * @param {string} props.movie Movie item to be displayed
	 * @param {array} props.config Configuration of The Movie Database API
	 */
	constructor(props) {
		super(props);
		this.state = {
			movie: props.item,
			config: props.config,
		}
	}

	render() {
		return (
			<div className="detail-show">
				<div className="row">
					<div className="col-lg-8">
						<div className="detail-text">
							<h1>{this.state.movie.title}</h1>
							<span className="tagline font-weight-lighter">{this.state.movie.tagline}</span>

							<p className="text-justify">
								{this.state.movie.overview}
							</p>

							<h2>More info</h2>
							<ul>
								<li><strong>Status:</strong> {this.state.movie.status}</li>
								<li><strong>Release date:</strong> {this.state.movie.release_date}</li>
								<li><strong>Original language:</strong> {this.state.movie.original_language}</li>
								<li><strong>Vote average:</strong> {this.state.movie.vote_average} (by {this.state.movie.vote_count} voters)</li>
								<li><strong>Homepage:</strong> <a href={this.state.movie.homepage}>{this.state.movie.homepage}</a></li>
							</ul>

							<VideoModalWindow title={this.state.movie.title} />
						</div>
					</div>

					<div className="col-lg-4">
						<div className="img-container">
							<ResponsiveImage item={this.state.movie} config={this.state.config} classes="h-100 d-inline-block img-fluid" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ShowMovie;