import React, { PureComponent } from 'react';

var player = null;

/**
 * This class creates modal window and integrating Shaka player
 */
class VideoModalWindow extends PureComponent {

	/**
	 * Class constructor
	 * @param {*} props Class properties
	 */
	constructor(props) {
		super(props);

		this.state = {
			width: 0,
			title: props.title,
		}

		// Bind open and close function
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);

		//Creating reference to store video component on DOM
		this.videoComponent = React.createRef();

		//Creating reference to store video container on DOM
		this.videoContainer = React.createRef();

		//Initializing reference to error handlers
		this.onErrorEvent = this.onErrorEvent.bind(this);
		this.onError = this.onError.bind(this);
	}

	onErrorEvent(event) {
		// Extract the shaka.util.Error object from the event.
		this.onError(event.detail);
	}

	onError(error) {
		// Log the error.
		console.error('Error code', error.code, 'object', error);
	}

	openModal = () => {
		// Handle creating shaka player component the window is defined
		if (typeof window !== 'undefined') {
			const shaka = require('shaka-player/dist/shaka-player.ui.js');


			//Link to MPEG-DASH video
			var manifestUri = 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8';

			shaka.polyfill.installAll();
			if (!shaka.Player.isBrowserSupported()) {
				console.log("Unsupported browser");
			}


			//Getting reference to video and video container on DOM
			const video = this.videoComponent.current;
			const videoContainer = this.videoContainer.current;

			//Initialize shaka player
			player = new shaka.Player(video);

			//Setting UI configuration JSON object
			const uiConfig = {};

			//Configuring elements to be displayed on video player control panel
			uiConfig['controlPanelElements'] = ['mute', 'volume', 'time_and_duration', 'fullscreen', 'overflow_menu',];

			//Setting up shaka player UI
			const ui = new shaka.ui.Overlay(player, videoContainer, video);
			ui.configure(ui);
			ui.getControls();

			// Listen for errors
			player.addEventListener('error', this.onErrorEvent);

			// Try to load a manifest.
			// This is an asynchronous process.
			player.load(manifestUri).then(function () {
				// This runs if the asynchronous load is successful.
				console.log('The video has now been loaded!');
			}).catch(this.onError);

			this.setState({
				width: '100%'
			});
		}
	}

	closeModal = () => {
		this.setState({
			width: 0
		});

		// Unload video on close
		player.unload();
		console.log('The video has now been unloaded');
	}

	render() {
		return (
			<div>
				<button className="btn btn-outline-secondary"
					onClick={this.openModal}>Play video</button>
				<div id="overlay" style={{ width: this.state.width }}>
					<div className="modal-title-bar">
						<h2>{this.props.title}</h2>
						<button className="close-btn" onClick={this.closeModal}>&times;</button>
					</div>
					<div className="overlay-content">
						<div className="video-container" ref={this.videoContainer}>
							<video
								className="shaka-video"
								ref={this.videoComponent}
							/>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default VideoModalWindow;