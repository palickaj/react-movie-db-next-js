import React, { Component } from 'react';
import Link from 'next/link';

import ResponsiveImage from './ResponsiveImage';

/**
 * This component creates list of movies with posters and names
 */
class List extends Component {
	/**
	 * Class constructor
	 * @param {*} props Class properties
	 * @param {string} props.title Title of listed cathegory
	 * @param {array} props.movieList List of movies in cathegory
	 * @param {array} props.config Configuration of The Movie Database API
	 * @param {string} props.type Type of cathegory (movie, serie)
	 */
	constructor(props) {
		super(props);
		this.state = {
			title: props.title,
			movieList: props.movieList,
			config: props.config,
			type: props.type,
		}
	}

	render() {
		const movieList = this.state.movieList.slice(0, 8);

		return (
			<div className="row carousel">
				<div className="col-12">
					<h1>{this.props.title}</h1>
				</div>
				{movieList.map((movie, index) => (
					<div className="col-12 col-sm-12 col-md-3 col-lg-4 col-xl item" key={index}>
						<Link href={`/[type]/[id]`} as={`/${this.state.type}/${movie.id}`}>
							<a>
								<ResponsiveImage item={movie} config={this.state.config} />
								<p className="title">{movie.title !== undefined ? movie.title : movie.name}</p>
							</a>
						</Link>
					</div>
				))}
			</div>
		)
	}
}

export default List;