import Head from 'next/head';
import Navbar from './Navbar';


/**
 * This component create layout of page and also includes definition of Head element
 *
 * @param {*} props
 */
const Layout = props => (
	<>
		<Head>
			<title>The Movie DB with React</title>
			<meta
				name="viewport"
				content="initial-scale=1.0, width=device-width"
				key="viewport"
			/>
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous" />
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossOrigin="anonymous" />
			<link href="/static/styles.css" rel="stylesheet" />
		</Head>
		<Navbar />
		<div className="container-fluid">
			{props.children}
		</div>
	</>
);

export default Layout;