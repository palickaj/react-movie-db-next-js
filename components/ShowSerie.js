import React, { Component } from 'react';
import VideoModalWindow from './VideoModalWindow';

import ResponsiveImage from './ResponsiveImage';

/**
 * This class creates component handling Serie info page
 */
class ShowSerie extends Component {
	/**
	 * Class constructor
	 * @param {*} props Class properties
	 * @param {string} props.serie Serie item to be displayed
	 * @param {array} props.config Configuration of The Movie Database API
	 */
	constructor(props) {
		super(props);
		this.state = {
			serie: props.item,
			config: props.config,
		}
	}

	render() {
		return (
			<div className="detail-show">
				<div className="row">
					<div className="col-lg-8">
						<div className="detail-text">
							<h1 className="with-margin">{this.state.serie.name}</h1>

							<p className="text-justify">
								{this.state.serie.overview}
							</p>

							<h2>More info</h2>
							<ul>
								<li><strong>Status:</strong> {this.state.serie.status}</li>
								<li><strong>Release date:</strong> {this.state.serie.first_air_date}</li>
								<li><strong>Original language:</strong> {this.state.serie.original_language}</li>
								<li><strong>Number of episodes:</strong> {this.state.serie.number_of_episodes}</li>
								<li><strong>Number of seasons:</strong> {this.state.serie.number_of_seasons}</li>
								<li><strong>Vote average:</strong> {this.state.serie.vote_average} (by {this.state.serie.vote_count} voters)</li>
								<li><strong>Homepage:</strong> <a href={this.state.serie.homepage}>{this.state.serie.homepage}</a></li>
							</ul>

							<VideoModalWindow title={this.state.serie.name} />
						</div>
					</div>

					<div className="col-lg-4">
						<div className="img-container">
						<ResponsiveImage item={this.state.serie} config={this.state.config} classes="h-100 d-inline-block img-fluid" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ShowSerie;