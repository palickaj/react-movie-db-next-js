import React from 'react'

/**
 * This component creates error box
 */
const ErrorBox = () => {

	return (
		<div className="loadingBox">
			<span><i className="far fa-dizzy"></i> Error occured</span>
		</div>
	)
};

export default ErrorBox;