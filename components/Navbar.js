import Link from 'next/link';

/**
 * This component creates main navigation bar at the top of the screen
 */
const Navbar = () => (
	<nav className="navbar navbar-expand-lg navbar-light bg-light">
		<div className="container-fluid">
			<ul className="navbar-nav">
				<li className="nav-item">
					<Link href="/">
						<a  className="nav-link">Home</a>
					</Link>
				</li>
			</ul>
			<ul className="nav navbar-nav navbar-right">
				<li>
					<Link href="/search">
						<a className="nav-link">Search</a>
					</Link>
				</li>
			</ul>
		</div>
	</nav>
);

export default Navbar;