/**
 * This compomemt creates loading animation
 */
const LoadingBox = () => (
	<div className="loadingBox">
		<span><i className="fas fa-spinner fa-pulse"></i> Loading...</span>
	</div>
);

export default LoadingBox;