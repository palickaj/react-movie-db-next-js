import { Component } from "react";

/**
 * This component handles creating responsive img element
 */
class ResponsiveImage extends Component {
	/**
	 * Class constructor
	 * @param {*} props Class properties
	 * @param {string} props.item Item with poster to by displayed as responsive image
	 * @param {array} props.config Configuration of The Movie Database API
	 * @param {string} props.classes Image element extra classes
	 */
	constructor(props) {
		super(props);

		this.state = {
			movie: props.item,
			config: props.config,
			classes: props.classes,
		}
	}

	render() {
		const baseImgPath = this.state.config.images.base_url;

		if (this.state.movie.poster_path === null) {
			return (
				<img src="/images/no-image.svg"
					alt={this.state.movie.title !== undefined ? this.state.movie.title : this.state.movie.name}
					className="no-img" />
			);
		} else {
			return (
				<img srcSet={baseImgPath + "w92/" + this.state.movie.poster_path + " 92w,\n" +
					baseImgPath + "w154/" + this.state.movie.poster_path + " 154w," +
					baseImgPath + "w185/" + this.state.movie.poster_path + " 185w," +
					baseImgPath + "w342/" + this.state.movie.poster_path + " 342w," +
					baseImgPath + "w500/" + this.state.movie.poster_path + " 500w," +
					baseImgPath + "w780/" + this.state.movie.poster_path + " 780w," +
					baseImgPath + "original/" + this.state.movie.poster_path + " 900w"}

					sizes="(max-width: 100px) 92px,
							(max-width: 154px) 154px,
							(max-width: 185px) 185px,
							(max-width: 342px) 342px,
							(max-width: 500px) 500px,
							(max-width: 780px) 780px,
							900px"

					src={baseImgPath + "original/" + this.state.movie.poster_path}
					alt={this.state.movie.title !== undefined ? this.state.movie.title : this.state.movie.name}
					className={this.state.classes !== null ? this.state.classes : undefined}
					/>
			)
		}
	}
}

export default ResponsiveImage;